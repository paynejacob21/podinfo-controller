package controller_test

import (
	"context"
	. "github.com/onsi/ginkgo/v2"
	. "github.com/onsi/gomega"
	"gitlab.com/paynejacob21/podinfo-controller/api/v1alpha1"
	"gitlab.com/paynejacob21/podinfo-controller/internal/controller"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sclient "sigs.k8s.io/controller-runtime/pkg/client"
	"time"
)

const (
	timeout  = time.Minute * 2
	interval = time.Millisecond * 250
)

var _ = Describe("PodInfoInstanceController", func() {
	ctx := context.Background()

	Context("Redis Disabled", func() {
		replicaCount := int32(1)
		sut := v1alpha1.PodInfoInstance{
			ObjectMeta: metav1.ObjectMeta{
				GenerateName: "test-",
				Namespace:    "default",
			},
			Spec: v1alpha1.PodInfoInstanceSpec{
				ReplicaCount: &replicaCount,
				Resources: v1alpha1.Resources{
					MemoryLimit: resource.MustParse("64Mi"),
					CpuRequest:  resource.MustParse("100m"),
				},
				ImageSpec: v1alpha1.ImageSpec{
					Repository: "ghcr.io/stefanprodan/podinfo",
					Tag:        "6.3.6",
				},
				UISpec: v1alpha1.UISpec{
					Color:   "#FFF",
					Message: "test-message",
				},
				RedisSpec: v1alpha1.RedisSpec{
					Enabled: false,
				},
			},
		}

		It("should create a PodInfoInstance", func() {
			Expect(k8sClient.Create(ctx, &sut)).Should(Succeed())
		})

		It("eventually is ready", func() {
			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionTrue(sut.Status.Conditions, controller.ReadyCondition)
			}, timeout, interval).Should(BeTrue())
		})

		It("detects component failures", func() {
			var target appsv1.Deployment
			Expect(k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name, Namespace: sut.Namespace}, &target)).Should(Succeed())
			Expect(k8sClient.Delete(ctx, &target)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionFalse(sut.Status.Conditions, controller.ReadyCondition)
			}, timeout, interval).Should(BeTrue())
		})

		It("should recover from component failures", func() {
			var target appsv1.Deployment

			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionTrue(sut.Status.Conditions, controller.ReadyCondition)
			}, timeout, interval).Should(BeTrue())

			Expect(k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name, Namespace: sut.Namespace}, &target)).Should(Succeed())
		})

		It("should delete a PodInfoInstance and all associated resources", func() {
			Expect(k8sClient.Delete(ctx, &sut)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				var target corev1.Service
				err := k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name, Namespace: sut.Namespace}, &target)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				var target appsv1.Deployment
				err := k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name, Namespace: sut.Namespace}, &target)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())
		})
	})

	Context("Redis Enabled", func() {
		replicaCount := int32(1)
		sut := v1alpha1.PodInfoInstance{
			ObjectMeta: metav1.ObjectMeta{
				GenerateName: "test-",
				Namespace:    "default",
			},
			Spec: v1alpha1.PodInfoInstanceSpec{
				ReplicaCount: &replicaCount,
				Resources: v1alpha1.Resources{
					MemoryLimit: resource.MustParse("64Mi"),
					CpuRequest:  resource.MustParse("100m"),
				},
				ImageSpec: v1alpha1.ImageSpec{
					Repository: "ghcr.io/stefanprodan/podinfo",
					Tag:        "6.3.6",
				},
				UISpec: v1alpha1.UISpec{
					Color:   "#FFF",
					Message: "test-message",
				},
				RedisSpec: v1alpha1.RedisSpec{
					Enabled: true,
					ImageSpec: v1alpha1.ImageSpec{
						Repository: "redis",
						Tag:        "7.0.11",
					},
					Storage: "5Gi",
				},
			},
		}

		It("should create a PodInfoInstance", func() {
			Expect(k8sClient.Create(ctx, &sut)).Should(Succeed())
		})

		It("eventually configures redis", func() {
			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionTrue(sut.Status.Conditions, controller.RedisReadyCondition)
			}, timeout, interval).Should(BeTrue())
		})

		It("eventually is ready", func() {
			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionTrue(sut.Status.Conditions, controller.ReadyCondition)
			}, timeout, interval).Should(BeTrue())
		})

		It("detects component failures", func() {
			var target appsv1.StatefulSet
			Expect(k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name + "-redis", Namespace: sut.Namespace}, &target)).Should(Succeed())
			Expect(k8sClient.Delete(ctx, &target)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionFalse(sut.Status.Conditions, controller.RedisReadyCondition)
			}, timeout, interval).Should(BeTrue())
		})

		It("should recover from component failures", func() {
			var target appsv1.StatefulSet
			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				if err != nil {
					return false
				}

				return meta.IsStatusConditionTrue(sut.Status.Conditions, controller.RedisReadyCondition)
			}, timeout, interval).Should(BeTrue())

			Expect(k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name + "-redis", Namespace: sut.Namespace}, &target)).Should(Succeed())
		})

		It("should delete a PodInfoInstance and all associated resources", func() {
			Expect(k8sClient.Delete(ctx, &sut)).Should(Succeed())

			Eventually(func() bool {
				err := k8sClient.Get(ctx, k8sclient.ObjectKeyFromObject(&sut), &sut)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				var target corev1.Service
				err := k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name, Namespace: sut.Namespace}, &target)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				var target appsv1.Deployment
				err := k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name, Namespace: sut.Namespace}, &target)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				var target corev1.Service
				err := k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name + "-redis", Namespace: sut.Namespace}, &target)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())

			Eventually(func() bool {
				var target appsv1.StatefulSet
				err := k8sClient.Get(ctx, k8sclient.ObjectKey{Name: sut.Name + "-redis", Namespace: sut.Namespace}, &target)
				return errors.IsNotFound(err)

			}, timeout, interval).Should(BeTrue())
		})
	})
})
