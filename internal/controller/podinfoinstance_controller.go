/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controller

import (
	"context"
	"fmt"
	errors2 "github.com/pkg/errors"
	infrav1alpha1 "gitlab.com/paynejacob21/podinfo-controller/api/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/log"
	"time"
)

const ReadyCondition = "Ready"
const RedisReadyCondition = "RedisReady"

// PodInfoInstanceReconciler reconciles a PodInfoInstance object
type PodInfoInstanceReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=infra.example.com,resources=podinfoinstances,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infra.example.com,resources=podinfoinstances/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=infra.example.com,resources=podinfoinstances/finalizers,verbs=update
//+kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps,resources=deployments/status,verbs=get
//+kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps,resources=statefulsets/status,verbs=get
//+kubebuilder:rbac:groups="",resources=services,verbs=get;list;watch;create;update;patch;delete

func (r *PodInfoInstanceReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	var podInfoInstance infrav1alpha1.PodInfoInstance
	var redisService *corev1.Service

	if err := r.Get(ctx, client.ObjectKey{Namespace: req.Namespace, Name: req.Name}, &podInfoInstance); err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}

		return ctrl.Result{Requeue: true}, err
	}

	if podInfoInstance.DeletionTimestamp != nil {
		return ctrl.Result{}, nil
	}

	// the presence of a redis condition indicates resources related to redis exist, if the condition does not exist but redis is enabled we need to create the resources
	if cond := meta.FindStatusCondition(podInfoInstance.Status.Conditions, RedisReadyCondition); cond != nil || podInfoInstance.Spec.RedisSpec.Enabled {
		redisService = &corev1.Service{
			ObjectMeta: metav1.ObjectMeta{
				Name:      req.Name + "-redis",
				Namespace: req.Namespace,
			},
		}

		if err := r.reconcileRedisService(ctx, &podInfoInstance, redisService); err != nil {
			return ctrl.Result{Requeue: true}, err
		}

		redisStatefulSet := appsv1.StatefulSet{
			ObjectMeta: metav1.ObjectMeta{
				Name:      req.Name + "-redis",
				Namespace: req.Namespace,
			},
		}

		if err := r.reconcileRedisStatefulSet(ctx, &podInfoInstance, redisService, &redisStatefulSet); err != nil {
			return ctrl.Result{Requeue: true}, err
		}

		if podInfoInstance.Spec.RedisSpec.Enabled {
			if redisStatefulSet.Status.CurrentRevision != redisStatefulSet.Status.UpdateRevision || redisStatefulSet.Status.AvailableReplicas != *redisStatefulSet.Spec.Replicas {
				meta.SetStatusCondition(&podInfoInstance.Status.Conditions, metav1.Condition{
					Type:               ReadyCondition,
					Status:             metav1.ConditionFalse,
					ObservedGeneration: podInfoInstance.Generation,
					LastTransitionTime: metav1.Time{Time: time.Now()},
					Reason:             "Waiting",
					Message:            "waiting for redis",
				})
				meta.SetStatusCondition(&podInfoInstance.Status.Conditions, metav1.Condition{
					Type:               RedisReadyCondition,
					Status:             metav1.ConditionFalse,
					ObservedGeneration: podInfoInstance.Generation,
					LastTransitionTime: metav1.Time{Time: time.Now()},
					Reason:             "Waiting",
					Message:            "waiting for changes to apply",
				})
				return ctrl.Result{}, r.Status().Update(ctx, &podInfoInstance)
			} else {
				meta.SetStatusCondition(&podInfoInstance.Status.Conditions, metav1.Condition{
					Type:               RedisReadyCondition,
					Status:             metav1.ConditionTrue,
					ObservedGeneration: podInfoInstance.Generation,
					LastTransitionTime: metav1.Time{Time: time.Now()},
					Reason:             "Ready",
					Message:            "",
				})
			}
		} else {
			if redisService.UID == "" && redisStatefulSet.UID == "" {
				// if redis is disabled, remove the condition as there is nothing to track
				meta.RemoveStatusCondition(&podInfoInstance.Status.Conditions, RedisReadyCondition)
			}
		}
	}

	deployment := appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      podInfoInstance.Name,
			Namespace: podInfoInstance.Namespace,
		},
	}

	if err := r.reconcileDeployment(ctx, &podInfoInstance, &deployment, redisService); err != nil {
		return ctrl.Result{Requeue: true}, err
	}

	var deploymentReady bool
	for _, condition := range deployment.Status.Conditions {
		if condition.Type == appsv1.DeploymentAvailable {
			deploymentReady = condition.Status == corev1.ConditionTrue
			break
		}
	}
	if !deploymentReady {
		meta.SetStatusCondition(&podInfoInstance.Status.Conditions, metav1.Condition{
			Type:               ReadyCondition,
			Status:             metav1.ConditionFalse,
			ObservedGeneration: podInfoInstance.Generation,
			LastTransitionTime: metav1.Time{Time: time.Now()},
			Reason:             "Waiting",
			Message:            "waiting for deployment",
		})

		return ctrl.Result{}, r.Status().Update(ctx, &podInfoInstance)
	}

	svc := corev1.Service{
		ObjectMeta: metav1.ObjectMeta{
			Name:      podInfoInstance.Name,
			Namespace: podInfoInstance.Namespace,
		},
	}

	if err := r.reconcileService(ctx, &podInfoInstance, &svc); err != nil {
		return ctrl.Result{Requeue: true}, err
	}

	if meta.IsStatusConditionFalse(podInfoInstance.Status.Conditions, ReadyCondition) {
		meta.SetStatusCondition(&podInfoInstance.Status.Conditions, metav1.Condition{
			Type:               ReadyCondition,
			Status:             metav1.ConditionTrue,
			ObservedGeneration: podInfoInstance.Generation,
			LastTransitionTime: metav1.Time{Time: time.Now()},
			Reason:             "Ready",
			Message:            "",
		})

		podInfoInstance.Status.ServiceURL = fmt.Sprintf("http://%s.%s", svc.Name, svc.Namespace)

		return ctrl.Result{}, r.Status().Update(ctx, &podInfoInstance)
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *PodInfoInstanceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&infrav1alpha1.PodInfoInstance{}).
		Owns(&appsv1.Deployment{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&corev1.Service{}).
		Complete(r)
}

func (r *PodInfoInstanceReconciler) reconcileService(ctx context.Context, podInfoInstance *infrav1alpha1.PodInfoInstance, svc *corev1.Service) error {
	if err := r.Get(ctx, client.ObjectKeyFromObject(svc), svc); client.IgnoreNotFound(err) != nil {
		return err
	}

	// no changes are needed if the svc exists and is up-to-date
	if !svc.CreationTimestamp.IsZero() && !objectStale(podInfoInstance, svc) {
		return nil
	}

	if err := r.setOwnership(podInfoInstance, svc); err != nil {
		return errors2.Wrap(err, "failed to set ownership on podinfo service")
	}

	svc.Spec.Type = corev1.ServiceTypeClusterIP
	svc.Spec.Selector = map[string]string{
		"podinfoinstance.example.com/name":       podInfoInstance.Name,
		"podinfoinstance.example.com/generation": fmt.Sprintf("%d", podInfoInstance.GetGeneration()),
		"app":                                    "podinfo",
	}
	svc.Spec.Ports = []corev1.ServicePort{
		{
			Name:       "http",
			Port:       80,
			TargetPort: intstr.IntOrString{Type: intstr.String, StrVal: "http"},
		},
	}

	if svc.GetUID() == "" {
		if err := r.Create(ctx, svc); err != nil {
			return errors2.Wrap(err, "failed to create podinfo service")
		}
	} else {
		if err := r.Update(ctx, svc); err != nil {
			return errors2.Wrap(err, "failed to update podinfo service")
		}
	}

	return nil
}

func (r *PodInfoInstanceReconciler) reconcileDeployment(ctx context.Context, podInfoInstance *infrav1alpha1.PodInfoInstance, deployment *appsv1.Deployment, redisService *corev1.Service) error {
	if err := r.Get(ctx, client.ObjectKeyFromObject(deployment), deployment); client.IgnoreNotFound(err) != nil {
		return err
	}

	// no changes are needed if the deployment exists and is up-to-date
	if !deployment.CreationTimestamp.IsZero() && !objectStale(podInfoInstance, deployment) {
		return nil
	}

	if err := r.setOwnership(podInfoInstance, deployment); err != nil {
		return errors2.Wrap(err, "failed to set ownership on pod info deployment")
	}

	deployment.Spec.Replicas = podInfoInstance.Spec.ReplicaCount
	deployment.Spec.Strategy = appsv1.DeploymentStrategy{
		Type: appsv1.RollingUpdateDeploymentStrategyType,
	}
	deployment.Spec.Selector = &metav1.LabelSelector{
		MatchLabels: map[string]string{
			"podinfoinstance.example.com/name": podInfoInstance.Name,
			"app":                              "podinfo",
		},
	}

	var env []corev1.EnvVar
	if podInfoInstance.Spec.RedisSpec.Enabled {
		env = append(env, corev1.EnvVar{
			Name:      "PODINFO_CACHE_SERVER",
			Value:     fmt.Sprintf("tcp://%s:6379", redisService.Name),
			ValueFrom: nil,
		})
	}
	if podInfoInstance.Spec.Color != "" {
		env = append(env, corev1.EnvVar{
			Name:      "PODINFO_UI_COLOR",
			Value:     podInfoInstance.Spec.Color,
			ValueFrom: nil,
		})
	}
	if podInfoInstance.Spec.Message != "" {
		env = append(env, corev1.EnvVar{
			Name:      "PODINFO_UI_MESSAGE",
			Value:     podInfoInstance.Spec.Message,
			ValueFrom: nil,
		})
	}

	resources := corev1.ResourceRequirements{
		Requests: map[corev1.ResourceName]resource.Quantity{
			corev1.ResourceCPU: podInfoInstance.Spec.Resources.CpuRequest,
		},
		Limits: map[corev1.ResourceName]resource.Quantity{
			corev1.ResourceMemory: podInfoInstance.Spec.Resources.MemoryLimit,
		},
	}

	deployment.Spec.MinReadySeconds = int32(5)
	deployment.Spec.Template = corev1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: deployment.Name,
			Labels: map[string]string{
				"podinfoinstance.example.com/name":       podInfoInstance.Name,
				"podinfoinstance.example.com/generation": fmt.Sprintf("%d", podInfoInstance.GetGeneration()),
				"app":                                    "podinfo",
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:      "podinfo",
					Image:     fmt.Sprintf("%s:%s", podInfoInstance.Spec.ImageSpec.Repository, podInfoInstance.Spec.Tag),
					Resources: resources,
					Env:       env,
					Ports: []corev1.ContainerPort{
						{
							Name:          "http",
							ContainerPort: 9898,
						},
					},
					LivenessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							HTTPGet: &corev1.HTTPGetAction{
								Path: "/healthz",
								Port: intstr.IntOrString{Type: intstr.Int, IntVal: 9898},
							},
						},
						InitialDelaySeconds: 5,
						TimeoutSeconds:      5,
					},
					ReadinessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							HTTPGet: &corev1.HTTPGetAction{
								Path: "/readyz",
								Port: intstr.IntOrString{Type: intstr.Int, IntVal: 9898},
							},
						},
						InitialDelaySeconds: 5,
						TimeoutSeconds:      5,
					},
					ImagePullPolicy: corev1.PullIfNotPresent,
				},
			},
			RestartPolicy: corev1.RestartPolicyAlways,
		},
	}

	if deployment.GetUID() == "" {
		if err := r.Create(ctx, deployment); err != nil {
			return errors2.Wrap(err, "failed to create podinfo deployment")
		}
	} else {
		if err := r.Update(ctx, deployment); err != nil {
			return errors2.Wrap(err, "failed to update podinfo deployment")
		}
	}

	return nil
}

func (r *PodInfoInstanceReconciler) reconcileRedisService(ctx context.Context, podInfoInstance *infrav1alpha1.PodInfoInstance, svc *corev1.Service) error {
	if err := r.Get(ctx, client.ObjectKeyFromObject(svc), svc); client.IgnoreNotFound(err) != nil {
		return err
	}

	// simply cleanup the object if it should not exist
	if !podInfoInstance.Spec.RedisSpec.Enabled {
		if err := r.Delete(ctx, svc); client.IgnoreNotFound(err) != nil {
			return err
		}

		return nil
	}

	// no changes are needed if the svc exists and is up-to-date
	if !svc.CreationTimestamp.IsZero() && !objectStale(podInfoInstance, svc) {
		return nil
	}

	if err := r.setOwnership(podInfoInstance, svc); err != nil {
		return errors2.Wrap(err, "failed to set ownership on redis service")
	}

	svc.Spec.ClusterIP = corev1.ClusterIPNone
	svc.Spec.Ports = []corev1.ServicePort{
		{
			Name:       "redis",
			Port:       6379,
			TargetPort: intstr.IntOrString{Type: intstr.String, StrVal: "redis"},
		},
	}

	svc.Spec.Selector = map[string]string{
		"podinfoinstance.example.com/name": podInfoInstance.Name,
		"app":                              "redis",
	}

	if svc.GetUID() == "" {
		if err := r.Create(ctx, svc); err != nil {
			return errors2.Wrap(err, "failed to create redis service")
		}
	} else {
		if err := r.Update(ctx, svc); err != nil {
			return errors2.Wrap(err, "failed to update redis service")
		}
	}

	return nil
}

func (r *PodInfoInstanceReconciler) reconcileRedisStatefulSet(ctx context.Context, podInfoInstance *infrav1alpha1.PodInfoInstance, service *corev1.Service, statefulSet *appsv1.StatefulSet) error {
	if err := r.Get(ctx, client.ObjectKeyFromObject(statefulSet), statefulSet); client.IgnoreNotFound(err) != nil {
		return err
	}

	// simply cleanup the object if it should not exist
	if !podInfoInstance.Spec.RedisSpec.Enabled {
		if err := r.Delete(ctx, statefulSet); client.IgnoreNotFound(err) != nil {
			return err
		}

		return nil
	}

	// no changes are needed if the object exists and is up-to-date
	if !statefulSet.CreationTimestamp.IsZero() && !objectStale(podInfoInstance, statefulSet) {
		return nil
	}

	if err := r.setOwnership(podInfoInstance, statefulSet); err != nil {
		return err
	}

	statefulSet.Spec.Replicas = Ptr(int32(1))
	statefulSet.Spec.Selector = &metav1.LabelSelector{
		MatchLabels: map[string]string{
			"podinfoinstance.example.com/name": podInfoInstance.Name,
			"app":                              "redis",
		},
	}

	storageQuantity, err := resource.ParseQuantity(podInfoInstance.Spec.RedisSpec.Storage)
	if err != nil {
		return fmt.Errorf("redis storage size is invalid: %s", err)
	}

	statefulSet.Spec.VolumeClaimTemplates = []corev1.PersistentVolumeClaim{
		{
			ObjectMeta: metav1.ObjectMeta{
				Name: "data",
			},
			Spec: corev1.PersistentVolumeClaimSpec{
				AccessModes: []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
				Resources: corev1.ResourceRequirements{
					Requests: map[corev1.ResourceName]resource.Quantity{
						corev1.ResourceStorage: storageQuantity,
					},
				},
			},
		},
	}

	statefulSet.Spec.ServiceName = service.Name
	statefulSet.Spec.PersistentVolumeClaimRetentionPolicy = &appsv1.StatefulSetPersistentVolumeClaimRetentionPolicy{
		WhenDeleted: appsv1.DeletePersistentVolumeClaimRetentionPolicyType,
		WhenScaled:  appsv1.RetainPersistentVolumeClaimRetentionPolicyType,
	}
	statefulSet.Spec.Template = corev1.PodTemplateSpec{
		ObjectMeta: metav1.ObjectMeta{
			GenerateName: statefulSet.Name,
			Labels: map[string]string{
				"podinfoinstance.example.com/name": podInfoInstance.Name,
				"app":                              "redis",
			},
		},
		Spec: corev1.PodSpec{
			Containers: []corev1.Container{
				{
					Name:  "redis",
					Image: fmt.Sprintf("%s:%s", podInfoInstance.Spec.RedisSpec.ImageSpec.Repository, podInfoInstance.Spec.RedisSpec.ImageSpec.Tag),
					Ports: []corev1.ContainerPort{
						{
							Name:          "redis",
							ContainerPort: 6379,
						},
					},
					Resources: corev1.ResourceRequirements{},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      "data",
							MountPath: "/data",
						},
					},
					LivenessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							TCPSocket: &corev1.TCPSocketAction{
								Port: intstr.IntOrString{Type: intstr.String, StrVal: "redis"},
							},
						},
						InitialDelaySeconds: 20,
						TimeoutSeconds:      1,
						PeriodSeconds:       5,
						SuccessThreshold:    1,
						FailureThreshold:    5,
					},
					ReadinessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							Exec: &corev1.ExecAction{
								Command: []string{"redis-cli", "ping"},
							},
						},
						InitialDelaySeconds: 20,
						TimeoutSeconds:      15,
						PeriodSeconds:       5,
						SuccessThreshold:    1,
						FailureThreshold:    5,
					},
					ImagePullPolicy: corev1.PullIfNotPresent,
				},
			},
			RestartPolicy: corev1.RestartPolicyAlways,
		},
	}

	if statefulSet.GetUID() == "" {
		if err := r.Create(ctx, statefulSet); err != nil {
			return errors2.Wrap(err, "failed to create redis statefulset")
		}
	} else {
		if err := r.Update(ctx, statefulSet); err != nil {
			return errors2.Wrap(err, "failed to update redis statefulset")
		}
	}

	return nil
}

func (r *PodInfoInstanceReconciler) setOwnership(podInfoInstance *infrav1alpha1.PodInfoInstance, obj metav1.Object) error {
	labels := map[string]string{}
	for k, v := range obj.GetLabels() {
		labels[k] = v
	}
	labels["podinfoinstance.example.com/name"] = podInfoInstance.Name
	labels["podinfoinstance.example.com/generation"] = fmt.Sprintf("%d", podInfoInstance.GetGeneration())

	obj.SetLabels(labels)

	return controllerutil.SetControllerReference(podInfoInstance, obj, r.Scheme)
}

func Ptr[T any](v T) *T {
	return &v
}

func objectStale(podInfoInstance *infrav1alpha1.PodInfoInstance, obj metav1.Object) bool {
	if obj.GetLabels() == nil {
		return false
	}

	return obj.GetLabels()["podinfoinstance.example.com/generation"] != fmt.Sprintf("%d", podInfoInstance.GetGeneration())
}
