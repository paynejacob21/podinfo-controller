/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

type ImageSpec struct {
	Repository string `json:"repository,omitempty"`
	Tag        string `json:"tag,omitempty"`
}

type UISpec struct {
	Color   string `json:"color,omitempty"`
	Message string `json:"message,omitempty"`
}

type RedisSpec struct {
	Enabled bool `json:"enabled,omitempty"`

	ImageSpec ImageSpec `json:"image"`
	Storage   string    `json:"storage"`
}

type Resources struct {
	MemoryLimit resource.Quantity `json:"memoryLimit"`
	CpuRequest  resource.Quantity `json:"cpuRequest"`
}

// PodInfoInstanceSpec defines the desired state of PodInfoInstance
type PodInfoInstanceSpec struct {
	ReplicaCount *int32    `json:"replicaCount,omitempty"`
	Resources    Resources `json:"resources,omitempty"`

	ImageSpec `json:"image"`
	UISpec    `json:"ui"`
	RedisSpec `json:"redis,omitempty"`
}

// PodInfoInstanceStatus defines the observed state of PodInfoInstance
type PodInfoInstanceStatus struct {
	ServiceURL string             `json:"serviceURL,omitempty"`
	Conditions []metav1.Condition `json:"conditions,omitempty"`
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// PodInfoInstance is the Schema for the podinfoinstances API
type PodInfoInstance struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   PodInfoInstanceSpec   `json:"spec,omitempty"`
	Status PodInfoInstanceStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// PodInfoInstanceList contains a list of PodInfoInstance
type PodInfoInstanceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []PodInfoInstance `json:"items"`
}

func init() {
	SchemeBuilder.Register(&PodInfoInstance{}, &PodInfoInstanceList{})
}
